function style_transfer(varargin)
% load network
if ~exist('net', 'var')
    net = load('data/models/imagenet-vgg-verydeep-16.mat');
end

% Set paths
opts.prefix = 'style';
opts = vl_argparse(opts, varargin);
opts.expDir = fullfile('data', opts.prefix);

% Create directories
mkdir(opts.expDir);

% input for content
imageName1 = {'brad_pitt.jpg'};

% input for style
imageName2 = {'picasso_selfport1907.jpg'};

% setting
commonArgs = {'imageSize', [224 224 3], ...
              'TVbeta', 2, ...
              'lambdaTV', 1e-6, ...
              'beta', 2, ...
              'lambdaLb', 0, ...
              'contentLayer', {'conv4_2'}, ...
              'contentLayerWeights', [1e-9], ...
              'textureLayer', {'relu1_1','relu2_1', 'relu3_1', 'relu4_1', 'relu5_1'}, ...
              'textureLayerWeights', [1 1 1 1 1]*2e-11, ...
              'attributeLayer', {}, ...
              'attributeLayerWeights', [], ...
              'attributeTarget', {}, ...
              'attributeTargetWeights', [], ...
              'maxIter', 100, ...
              'useGPU', true, ...
              'textureInit', 'transfer-quilt'};



for i = 1:length(imageName1), 
    
    im1 = imread(imageName1{i});
    im1 = imresize(im1, [224 224]);
    
    im2 = imread(imageName2{i});
    im2 = imresize(im2, [224 224]);
    
    outFile = fullfile(opts.expDir, sprintf('%s-%s.png', ...
                                            imageName1{i}(1:end-4), imageName2{i}(1:end-4)));
    if exist(outFile, 'file');
        continue;
    end

    res = texture_syn(im1, im2, net, commonArgs);
    
    imwrite(gather(res.imsyn), outFile);
    if true,
        figure(1); clf;
        imshow(res.imsyn); axis image off;
    end
end
